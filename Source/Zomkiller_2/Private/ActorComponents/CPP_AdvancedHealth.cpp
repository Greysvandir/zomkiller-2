// Fill out your copyright notice in the Description page of Project Settings.


#include "ActorComponents/CPP_AdvancedHealth.h"
#include "Net/UnrealNetwork.h"

UCPP_AdvancedHealth::UCPP_AdvancedHealth()
	: UCPP_Health()
{
	//
}

void UCPP_AdvancedHealth::Damage(float Dmg)
{
	if (ShieldPoints > 0)
	{
		ShieldPoints--;
		//OnShieldDamage.Broadcast(ShieldPoints);
		OnShieldDamageBroadcast_Multicast(ShieldPoints);
	}
	else
	{
		Super::Damage(Dmg);
	}
}

void UCPP_AdvancedHealth::SetShieldPoints(int NewShieldPoints)
{
	ShieldPoints = NewShieldPoints;
}

int UCPP_AdvancedHealth::GetShieldPoints()
{
	return ShieldPoints;
}

void UCPP_AdvancedHealth::OnShieldDamageBroadcast_Multicast_Implementation(int NewShieldPoints)
{
	OnShieldDamage.Broadcast(NewShieldPoints);
}

void UCPP_AdvancedHealth::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// ���������, ��� ���������� SomeVariable ������ ���������������
	DOREPLIFETIME(UCPP_AdvancedHealth, ShieldPoints);
}