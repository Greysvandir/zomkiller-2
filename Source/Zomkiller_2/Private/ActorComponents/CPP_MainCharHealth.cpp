// Fill out your copyright notice in the Description page of Project Settings.


#include "ActorComponents/CPP_MainCharHealth.h"
#include "Kismet/GameplayStatics.h"
#include "Character/CPP_Character.h"

void UCPP_MainCharHealth::BeginPlay()
{
	Super::BeginPlay();
	ReplicateEvents = true;
}

void UCPP_MainCharHealth::Death_Multicast_Implementation()
{
	Super::Death_Multicast_Implementation();

	ACPP_Character* Player = Cast<ACPP_Character>(Owner);
	if (Player != nullptr)
	{
		//APlayerController* Controller = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		//ACharacter* Player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
		//Player->DisableInput(Controller);
		APlayerController* Controller = Cast<APlayerController>(Player->GetController());
		if (Controller != nullptr)
		{
			Controller->SetIgnoreMoveInput(true);
			Controller->SetIgnoreLookInput(true);
		}
	}
}
