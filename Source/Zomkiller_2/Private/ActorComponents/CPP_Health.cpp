// Fill out your copyright notice in the Description page of Project Settings.


#include "ActorComponents/CPP_Health.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UCPP_Health::UCPP_Health()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	
}


// Called when the game starts
void UCPP_Health::BeginPlay()
{
	Super::BeginPlay();

	HP = MaxHP;
	Owner = GetOwner();
	Owner->OnTakeAnyDamage.AddDynamic(this, &UCPP_Health::HandleAnyDamage);
	
}


// Called every frame
void UCPP_Health::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UCPP_Health::SpawnDamageWidget(float dmg)
{
	FVector NewLocation(0, 0, 0);
	NewLocation = Owner->GetActorLocation();
	FTransform NewTransform(NewLocation);
	//ACPP_DamageWidgetActor* DamageWidgetActor = GetWorld()->SpawnActor<ACPP_DamageWidgetActor>(DamageWidgetActorClass, NewTransform);
	//DamageWidgetActor->SetProperties(dmg, MaxHP, HP);
	//DamageWidgetActor->SetDamage(dmg, MaxHP, HP);
	SpawnDamageWidget_Multicast(NewTransform, dmg, HP);
}

void UCPP_Health::SpawnDamageWidget_Multicast_Implementation(FTransform NewTransform, float dmg, float NewHP)
{
	ACPP_DamageWidgetActor* DamageWidgetActor = GetWorld()->SpawnActor<ACPP_DamageWidgetActor>(DamageWidgetActorClass, NewTransform);
	//DamageWidgetActor->SetProperties(dmg, MaxHP, NewHP);
	DamageWidgetActor->SetDamage(dmg, MaxHP, NewHP);
}

void UCPP_Health::Damage(float Dmg)
{
	if (isDead) {
		return;
	}
	if (Dmg == 0.f)
	{
		return;
	}

	HP -= Dmg;
	if (HP <= 0.f)
	{
		HP = 0.f;
		if (ReplicateEvents)
		{
			OnDamageBroadcast_Multicast(HP);
		}
		else
		{
			OnDamage.Broadcast(HP);
		}
		
		Death();
	}
	else
	{
		if (ReplicateEvents)
		{
			OnDamageBroadcast_Multicast(HP);
		}
		else
		{
			OnDamage.Broadcast(HP);
		}
	}

	SpawnDamageWidget(Dmg);
}

void UCPP_Health::Heal(float Dmg)
{
	if (isDead) {
		return;
	}
	if (Dmg == 0.f)
	{
		return;
	}

	HP += Dmg;
	if (HP > MaxHP)
	{
		HP = MaxHP;
	}
	
	if (ReplicateEvents)
	{
		OnHealBroadcast_Multicast(HP);
	}
	else
	{
		OnHeal.Broadcast(HP);
	}
}

void UCPP_Health::Death()
{
	if (ReplicateEvents)
	{
		Death_Multicast();
	}
	else
	{
		if (isDead) {
			return;
		}

		isDead = true;
		OnDead.Broadcast();
	}
	//OnDeadBroadcast_Multicast();
}

void UCPP_Health::Death_Multicast_Implementation()
{
	if (isDead) {
		return;
	}

	isDead = true;
	OnDead.Broadcast();
}

bool UCPP_Health::GetIsDead()
{
	return isDead;
}

float UCPP_Health::GetHP()
{
	return HP;
}

void UCPP_Health::HandleAnyDamage(AActor* DamagedActor, float damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (damage > 0.f)
	{
		Damage(damage);
	}
	else
	{
		Heal(-damage);
	}
}

void UCPP_Health::OnDeadBroadcast_Multicast_Implementation()
{
	OnDead.Broadcast();
}

void UCPP_Health::OnHealBroadcast_Multicast_Implementation(float NewHP)
{
	OnHeal.Broadcast(NewHP);
}

void UCPP_Health::OnDamageBroadcast_Multicast_Implementation(float NewHP)
{
	OnDamage.Broadcast(NewHP);
}

void UCPP_Health::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// ���������, ��� ���������� SomeVariable ������ ���������������
	DOREPLIFETIME(UCPP_Health, HP);
	DOREPLIFETIME(UCPP_Health, isDead);
}