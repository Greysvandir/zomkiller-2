// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_StatesEffects.h"

bool UCPP_StatesEffects::SuitableSurface(TArray<TEnumAsByte<EPhysicalSurface>> ObjectSurfaces, TSubclassOf<UCPP_StatesEffects> NewState)
{
    UCPP_StatesEffects* MyStateInstance = NewState.GetDefaultObject();
    TArray<TEnumAsByte<EPhysicalSurface>> lAccessibleSurfaces = MyStateInstance->AccessibleSurfaces;

    for (const TEnumAsByte<EPhysicalSurface>& NewSurface : lAccessibleSurfaces)
    {
        if (ObjectSurfaces.Contains(NewSurface))
        {
            return true;
        }
    }

    return false;
}

void UCPP_StatesEffects::StartEffect(AActor* NewOwner)
{
    Owner = NewOwner;
    OwnerHealthComp = Cast<UCPP_Health>(Owner->GetComponentByClass(UCPP_Health::StaticClass()));

    if (ParticleEffect)
    {
        //ObjectEmitter = NewObject<ACPP_EffectEmitter>(this, ACPP_EffectEmitter::StaticClass());
        //ObjectEmitter->StartEmitterEffect(ParticleEffect, NewOwner);
        ObjectEmitter = Cast<ACPP_EffectEmitter>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, ACPP_EffectEmitter::StaticClass(), FTransform()));
        if (ObjectEmitter)
        {
            ObjectEmitter->ParticleEffect = ParticleEffect;
            ObjectEmitter->IOwner = NewOwner;
            UGameplayStatics::FinishSpawningActor(ObjectEmitter, FTransform());
        }
    }
}

void UCPP_StatesEffects::Action()
{
    if (OwnerHealthComp)
    {
        if (heal > 0)
        {
            OwnerHealthComp->Heal(heal * CurrentStack);
        }
        if (damage > 0)
        {
            OwnerHealthComp->Damage(damage * CurrentStack);
        }
    }
}

void UCPP_StatesEffects::DestroyObject(bool DoDelayDeactivate)
{
    if (ParticleEffect)
    {
        ObjectEmitter->StopEmitterEffect(DoDelayDeactivate);
        ObjectEmitter = nullptr;
    }

    ICPP_IHitEffect* Iface = Cast<ICPP_IHitEffect>(Owner);
    if (Iface != nullptr)
    {
        Iface->StopEffect(this->GetClass());
    }

    Owner = nullptr;
    if (this && this->IsValidLowLevel())
    {
        this->ConditionalBeginDestroy();
    }
}

bool UCPP_StatesEffects::ExecOnce()
{
    return false;
}

void UCPP_StatesEffects::UpdateEffect()
{
}

void UCPP_StatesEffects_ExecOnce::StartEffect(AActor* NewOwner)
{
    Super::StartEffect(NewOwner);

    Action();
    DestroyObject(true);
}

void UCPP_StatesEffects_ExecOnce::Action()
{
    Super::Action();
}

void UCPP_StatesEffects_ExecOnce::DestroyObject(bool DoDelayDeactivate)
{
    Super::DestroyObject(true);
}

bool UCPP_StatesEffects_ExecOnce::ExecOnce()
{
    return true;
}

void UCPP_StatesEffects_ExecTimer::StartEffect(AActor* NewOwner)
{
    Super::StartEffect(NewOwner);

    if (lifeTime <= 0)
    {
        DestroyObject(false);
    }
    else
    {
        GetWorld()->GetTimerManager().SetTimer(rateTimer, this, &UCPP_StatesEffects_ExecTimer::Action, rate, true);
        GetWorld()->GetTimerManager().SetTimer(lifeTimeTimer, this, &UCPP_StatesEffects_ExecTimer::DestroyObjectTimer, lifeTime, false);
    }
}

void UCPP_StatesEffects_ExecTimer::DestroyObject(bool DoDelayDeactivate)
{
    if (GetWorld()->GetTimerManager().IsTimerActive(lifeTimeTimer))
    {
        GetWorld()->GetTimerManager().ClearTimer(lifeTimeTimer);
    }
    if (GetWorld()->GetTimerManager().IsTimerActive(rateTimer))
    {
        GetWorld()->GetTimerManager().ClearTimer(rateTimer);
    }

    Super::DestroyObject(false);
}

void UCPP_StatesEffects_ExecTimer::DestroyObjectTimer()
{
    DestroyObject(false);
}

void UCPP_StatesEffects_ExecTimer::UpdateEffect()
{
    GetWorld()->GetTimerManager().ClearTimer(lifeTimeTimer);
    GetWorld()->GetTimerManager().SetTimer(lifeTimeTimer, this, &UCPP_StatesEffects_ExecTimer::DestroyObjectTimer, lifeTime, false);
}
