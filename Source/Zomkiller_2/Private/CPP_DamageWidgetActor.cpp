// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_DamageWidgetActor.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ACPP_DamageWidgetActor::ACPP_DamageWidgetActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//bReplicates = true;
}

// Called when the game starts or when spawned
void ACPP_DamageWidgetActor::BeginPlay()
{
	Super::BeginPlay();
	GetWorldTimerManager().SetTimer(AutoDestroyTimer, this, &ACPP_DamageWidgetActor::AutoDestroy, 0.5f, false);
}

// Called every frame
void ACPP_DamageWidgetActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACPP_DamageWidgetActor::SetProperties(float NewDamage, float NewMaxHP, float NewCurrentHP)
{
	Damage = NewDamage;
	MaxHP = NewMaxHP;
	CurrentHP = NewCurrentHP;
}

void ACPP_DamageWidgetActor::SetDamage_Implementation(float NewDamage, float NewMaxHP, float NewCurrentHP)
{

}

void ACPP_DamageWidgetActor::AutoDestroy()
{
	this->Destroy();
}

void ACPP_DamageWidgetActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// ���������, ��� ���������� SomeVariable ������ ���������������
	//DOREPLIFETIME(ACPP_DamageWidgetActor, Damage);
	//DOREPLIFETIME(ACPP_DamageWidgetActor, MaxHP);
	//DOREPLIFETIME(ACPP_DamageWidgetActor, CurrentHP);
}

