// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ActorComponents/CPP_Health.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "StatesEffects/CPP_EffectEmitter.h"
#include "Interfaces/CPP_IHitEffect.h"
#include "CPP_StatesEffects.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class ZOMKILLER_2_API UCPP_StatesEffects : public UObject
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffects")
	TArray<TEnumAsByte<EPhysicalSurface>> AccessibleSurfaces;

	AActor* Owner;
	UCPP_Health* OwnerHealthComp;
	int CurrentStack = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffects")
	float damage;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffects")
	float heal;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffects")
	int MaxStack = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffects")
	bool DoUpdate = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	UParticleSystem* ParticleEffect = nullptr;

	ACPP_EffectEmitter* ObjectEmitter;

	static bool SuitableSurface(TArray<TEnumAsByte<EPhysicalSurface>> ObjectSurfaces, TSubclassOf<UCPP_StatesEffects> NewState);
	virtual void StartEffect(AActor* NewOwner);
	virtual void Action();
	virtual void DestroyObject(bool DoDelayDeactivate);
	virtual bool ExecOnce();
	virtual void UpdateEffect();
};

UCLASS()
class ZOMKILLER_2_API UCPP_StatesEffects_ExecOnce : public UCPP_StatesEffects
{
	GENERATED_BODY()

public:
	void StartEffect(AActor* NewOwner) override;
	void Action() override;
	void DestroyObject(bool DoDelayDeactivate) override;
	bool ExecOnce() override;
};

UCLASS()
class ZOMKILLER_2_API UCPP_StatesEffects_ExecTimer : public UCPP_StatesEffects
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffects")
	float lifeTime = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffects")
	float rate = 0.f;

	FTimerHandle lifeTimeTimer;
	FTimerHandle rateTimer;

public:
	void StartEffect(AActor* NewOwner) override;
	void DestroyObject(bool DoDelayDeactivate) override;
	void DestroyObjectTimer();
	void UpdateEffect() override;
};