// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_DamageWidgetActor.generated.h"

UCLASS(Blueprintable)
class ZOMKILLER_2_API ACPP_DamageWidgetActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_DamageWidgetActor();

	FTimerHandle AutoDestroyTimer;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
	float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
	float MaxHP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
	float CurrentHP;

	UFUNCTION()
	void SetProperties(float NewDamage, float NewMaxHP, float NewCurrentHP);

	UFUNCTION(BlueprintNativeEvent)
	void SetDamage(float NewDamage, float NewMaxHP, float NewCurrentHP);
	void SetDamage_Implementation(float NewDamage, float NewMaxHP, float NewCurrentHP);

	void AutoDestroy();
};
