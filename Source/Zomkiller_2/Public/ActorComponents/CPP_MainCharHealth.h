// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ActorComponents/CPP_AdvancedHealth.h"
#include "CPP_MainCharHealth.generated.h"

/**
 * 
 */
UCLASS()
class ZOMKILLER_2_API UCPP_MainCharHealth : public UCPP_AdvancedHealth
{
	GENERATED_BODY()
	
public:

protected:

public:
	virtual void BeginPlay() override;
	virtual void Death_Multicast_Implementation() override;
};
