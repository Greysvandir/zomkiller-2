// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CPP_DamageWidgetActor.h"
#include "Components/ActorComponent.h"
#include "CPP_Health.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDamage, float, NewHealth);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHeal, float, NewHealth);

UCLASS(Blueprintable)
class ZOMKILLER_2_API UCPP_Health : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCPP_Health();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxHP = 100.0f;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnDead OnDead;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnDamage OnDamage;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnHeal OnHeal;

	bool ReplicateEvents = false;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	UPROPERTY(Replicated)
	float HP = 0.0f;

	UPROPERTY(Replicated)
	bool isDead = false;
	AActor* Owner;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ACPP_DamageWidgetActor> DamageWidgetActorClass;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SpawnDamageWidget(float dmg);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnDamageWidget_Multicast(FTransform NewTransform, float dmg, float NewHP);

	UFUNCTION(BlueprintCallable, Category = "Health")
	virtual void Damage(float Dmg);

	UFUNCTION(BlueprintCallable, Category = "Health")
	void Heal(float Dmg);

	UFUNCTION(BlueprintCallable, Category = "Health")
	void Death();

	UFUNCTION(NetMulticast, Reliable)
	virtual void Death_Multicast();

	UFUNCTION(BlueprintPure, Category = "Health")
	bool GetIsDead();

	UFUNCTION(BlueprintPure, Category = "Health")
	float GetHP();

	UFUNCTION()
	void HandleAnyDamage(AActor* DamagedActor, float damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION(NetMulticast, Reliable)
	void OnDamageBroadcast_Multicast(float NewHP);

	UFUNCTION(NetMulticast, Reliable)
	void OnHealBroadcast_Multicast(float NewHP);

	UFUNCTION(NetMulticast, Reliable)
	void OnDeadBroadcast_Multicast();
};
