// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ActorComponents/CPP_Health.h"
#include "CPP_AdvancedHealth.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnShieldDamage, int, NewShieldPoints);

UCLASS()
class ZOMKILLER_2_API UCPP_AdvancedHealth : public UCPP_Health
{
	GENERATED_BODY()
	
public:
	
	UCPP_AdvancedHealth();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnShieldDamage OnShieldDamage;

protected:

	UPROPERTY(Replicated)
	int ShieldPoints = 4;

public:

	void Damage(float Dmg) override;

	UFUNCTION(BlueprintCallable)
	void SetShieldPoints(int NewShieldPoints);

	UFUNCTION(BlueprintPure)
	int GetShieldPoints();

	UFUNCTION(NetMulticast, Reliable)
	void OnShieldDamageBroadcast_Multicast(int NewShieldPoints);
};
