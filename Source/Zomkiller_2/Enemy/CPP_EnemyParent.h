// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Interfaces/CPP_IHitEffect.h"
#include "StatesEffects/CPP_StatesEffectsCommonFunc.h"
#include "CPP_EnemyParent.generated.h"

UCLASS()
class ZOMKILLER_2_API ACPP_EnemyParent : public ACharacter, public ICPP_IHitEffect
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACPP_EnemyParent();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffects")
	TArray<TEnumAsByte<EPhysicalSurface>> ObjectSurfaces;

	TArray<FStateEffectStack> CurrentStates;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ExposeOnSpawn = "true"))
	bool isServer;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void StartEffect(TSubclassOf<UCPP_StatesEffects> NewState) override;
	void StopEffect(TSubclassOf<UCPP_StatesEffects> NewState) override;

};
