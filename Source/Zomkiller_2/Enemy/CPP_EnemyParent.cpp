// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy/CPP_EnemyParent.h"

// Sets default values
ACPP_EnemyParent::ACPP_EnemyParent()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACPP_EnemyParent::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACPP_EnemyParent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACPP_EnemyParent::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ACPP_EnemyParent::StartEffect(TSubclassOf<UCPP_StatesEffects> NewState)
{
	if (isServer)
	{
		UE_LOG(LogTemp, Warning, TEXT("!!!!!!!!!!!!!!!!!!!!!!!!!!! start 11"));
		UCPP_StatesEffectsCommonFunc::CheckSurfaceAndStartEffect(ObjectSurfaces, CurrentStates, NewState, this);
	}
}

void ACPP_EnemyParent::StopEffect(TSubclassOf<UCPP_StatesEffects> NewState)
{
	if (isServer)
	{
		UCPP_StatesEffectsCommonFunc::RemoveStatesByClass(CurrentStates, NewState);
	}
}