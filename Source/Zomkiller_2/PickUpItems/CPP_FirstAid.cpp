// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUpItems/CPP_FirstAid.h"

void ACPP_FirstAid::PickUp()
{
	ICPP_IHitEffect* Iface = Cast<ICPP_IHitEffect>(Player);
	if (Iface != nullptr)
	{
		Iface->StartEffect(PickUpEffect);
	}
}
