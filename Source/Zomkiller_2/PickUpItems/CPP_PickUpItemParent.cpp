// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUpItems/CPP_PickUpItemParent.h"

// Sets default values
ACPP_PickUpItemParent::ACPP_PickUpItemParent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box component"));
	BoxComponent->SetGenerateOverlapEvents(true);
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ACPP_PickUpItemParent::BoxComponentBeginOverlap);

	RootComponent = BoxComponent;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMesh->SetGenerateOverlapEvents(false);
	StaticMesh->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMesh->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ACPP_PickUpItemParent::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACPP_PickUpItemParent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACPP_PickUpItemParent::BoxComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Player = Cast<ACPP_Character>(OtherActor);
	if (Player != nullptr)
	{
		PickUp();
		this->Destroy();
	}
}

void ACPP_PickUpItemParent::PickUp()
{
}

