// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUpItems/CPP_PickUpItemParent.h"
#include "CPP_FirstAid.generated.h"

/**
 * 
 */
UCLASS()
class ZOMKILLER_2_API ACPP_FirstAid : public ACPP_PickUpItemParent
{
	GENERATED_BODY()
	
public:

	void PickUp() override;
};
