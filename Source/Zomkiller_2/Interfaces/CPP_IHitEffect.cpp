// Fill out your copyright notice in the Description page of Project Settings.


#include "Interfaces/CPP_IHitEffect.h"
#include "CPP_StatesEffects.h"

// Add default functionality here for any ICPP_IHitEffect functions that are not pure virtual.

void ICPP_IHitEffect::StartEffect(TSubclassOf<UCPP_StatesEffects> NewState)
{
}

void ICPP_IHitEffect::StopEffect(TSubclassOf<UCPP_StatesEffects> NewState)
{
}
