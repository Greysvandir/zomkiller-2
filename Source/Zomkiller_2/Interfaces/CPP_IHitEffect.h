// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
//#include "CPP_StatesEffects.h"
#include "CPP_IHitEffect.generated.h"

// Forward declaration
class UCPP_StatesEffects;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCPP_IHitEffect : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ZOMKILLER_2_API ICPP_IHitEffect
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual void StartEffect(TSubclassOf<UCPP_StatesEffects> NewState);
	virtual void StopEffect(TSubclassOf<UCPP_StatesEffects> NewState);

};
