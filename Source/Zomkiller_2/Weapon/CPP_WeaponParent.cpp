// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/CPP_WeaponParent.h"
#include "Components/BoxComponent.h"
#include "Components/ArrowComponent.h"
#include "Character/CPP_Character.h"
#include "Core/CPP_GI.h"
#include "Weapon/Projectile/CPP_ProjectileParent.h"
#include "Weapon/Sleeve/CPP_Sleeve_Parent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/CapsuleComponent.h"

#define TraceMouseCursor ETraceTypeQuery::TraceTypeQuery3

// Sets default values
ACPP_WeaponParent::ACPP_WeaponParent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box component"));
	BoxComponent->SetGenerateOverlapEvents(false);
	BoxComponent->SetCollisionProfileName(TEXT("NoCollision"));
	RootComponent = BoxComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
	
	SleeveLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("SleeveLocation"));
	SleeveLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ACPP_WeaponParent::BeginPlay()
{
	Super::BeginPlay();
	
	SetWeaponParameters();

}

// Called every frame
void ACPP_WeaponParent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Fire(DeltaTime);
}

void ACPP_WeaponParent::SetWeaponParameters()
{
	UCPP_GI* myGI = Cast<UCPP_GI>(GetGameInstance());
	FWeaponParameters tmpWeaponParameters;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, tmpWeaponParameters))
		{
			WeaponParameters = tmpWeaponParameters;
			isUnlimitAmmo = WeaponParameters.UnlimitAmmo;
		}
	}
}

void ACPP_WeaponParent::Equip(ACPP_Character* NewOwner, int NewAmmoCurrent, int NewAmmoAll)
{
	SetWeaponParameters(); // Equip ������ ���������� ����� �++ BeginPlay
	FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
	OwnerOfWeapon = NewOwner;
	this->AttachToComponent(OwnerOfWeapon->GetMesh(), Rule, EquipSocket);
	OwnerOfWeapon->CurrentWeapon = this;
	AmmoCurrent = NewAmmoCurrent;
	AmmoAll = NewAmmoAll;
	OwnerOfWeapon->WeaponEquipEvent();
}

void ACPP_WeaponParent::StartFire()
{
	StartFire_OnServer();
}

void ACPP_WeaponParent::StopFire()
{
	StopFire_OnServer();
}

void ACPP_WeaponParent::StartFire_OnServer_Implementation()
{
	StartFire_Multicast();
}

void ACPP_WeaponParent::StopFire_OnServer_Implementation()
{
	StopFire_Multicast();
}

void ACPP_WeaponParent::StartFire_Multicast_Implementation()
{
	if (inFire)
		return;

	inFire = true;
}

void ACPP_WeaponParent::StopFire_Multicast_Implementation()
{
	inFire = false;
}

void ACPP_WeaponParent::Fire(float DeltaTime)
{
	if (!isLocal)
	{
		return;
	}

	if (CooldownTime > 0)
	{
		CooldownTime -= DeltaTime;
		if (CooldownTime <= 0)
		{
			CooldownTime = 0;
			inCooldown = false;
		}
	}

	if (inFire && !inCooldown && !isReloading && AmmoCurrent > 0)
	{
		FVector FireLocation(ShootLocation->GetComponentLocation());
		FRotator FireRotation;

		FHitResult HitResult;
		UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHitResultUnderCursorByChannel(TraceMouseCursor, false, HitResult);

		if (FVector::Distance(GetActorLocation(), HitResult.Location) > 200.f)
		{
			FireRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location);
		}
		else
		{
			FireRotation = ShootLocation->GetComponentRotation();
		}
		FireRotation.Roll = 0.f;
		FireRotation.Pitch = 0.f;

		float Dispersion;
		float AimDispersionCoef = 1.f;
		if (!WeaponParameters.BlockAimDispersion)
		{
			if (OwnerOfWeapon->MovementState == EMovementState::Aim_State || OwnerOfWeapon->MovementState == EMovementState::AimWalk_State)
			{
				//���� �� � ������ ������������ ������� �������� ����������� ����� (����� ���������)
				AimDispersionCoef = 3.f;
			}
		}
		for (int i = 1; i <= WeaponParameters.BulletsPerFire; i++)
		{
			Dispersion = FMath::RandRange(-WeaponParameters.Dispersion / AimDispersionCoef, WeaponParameters.Dispersion / AimDispersionCoef);
			FireRotation.Yaw += Dispersion;

			FTransform FireTransform(FireRotation, FireLocation, FVector::OneVector);
			/*ACPP_ProjectileParent* Projectile = Cast<ACPP_ProjectileParent>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, WeaponParameters.BulletType, FireTransform));
			if (Projectile)
			{
				if (!WeaponParameters.UseLineTraceFire)
				{
					Projectile->ProjectileParameters.Damage = WeaponParameters.Damage;
				}
				Projectile->ProjectileParameters.Speed = WeaponParameters.BulletSpeed;
				Projectile->BulletProjectileMovement->InitialSpeed = WeaponParameters.BulletSpeed;
				Projectile->BulletProjectileMovement->MaxSpeed = WeaponParameters.BulletSpeed;
				Projectile->DamageDelay = WeaponParameters.DamageDelay;
				UGameplayStatics::FinishSpawningActor(Projectile, FireTransform);
			}*/
			Fire_OnServer(FireTransform);
		}
		
		//�������� �������� ����� �����
		if (WeaponParameters.UseLineTraceFire)
		{
			FVector EndTrace(FireLocation + FireRotation.Vector() * 50000.f);
			/*TArray<AActor*> IgnoreActors;
			FHitResult OutHit;
			if (UKismetSystemLibrary::LineTraceSingle(GetWorld(), FireLocation, EndTrace, ETraceTypeQuery::TraceTypeQuery1, false, IgnoreActors, EDrawDebugTrace::ForDuration, OutHit, true))
			{
				UGameplayStatics::ApplyDamage(OutHit.GetActor(), WeaponParameters.Damage, GetInstigatorController(), this, NULL);
			}*/
			LineTraceFire_OnServer(FireLocation, EndTrace);
		}

		inCooldown = true;
		CooldownTime = WeaponParameters.Cooldown;
		AmmoCurrent--;

		/*UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponParameters.FireSound, FireLocation);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponParameters.FireParticle, FTransform(FireRotation, FireLocation, FVector(1.0f)));

		//������
		FVector VSleeveLocation(SleeveLocation->GetComponentLocation());
		FTransform SleeveTransform(VSleeveLocation);
		ACPP_Sleeve_Parent* Sleeve = GetWorld()->SpawnActor<ACPP_Sleeve_Parent>(WeaponParameters.SleeveType, SleeveTransform);
		FRotator SleeveRotation = SleeveLocation->GetComponentRotation();
		Sleeve->Capsule->AddImpulse(SleeveRotation.Vector() * 50.f);*/

		FireEffects_OnServer(FireLocation, FireRotation);

		//������� ��������
		OwnerOfWeapon->WeaponFireEvent();
	}
}

void ACPP_WeaponParent::Fire_OnServer_Implementation(FTransform FireTransform)
{
	ACPP_ProjectileParent* Projectile = Cast<ACPP_ProjectileParent>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), WeaponParameters.BulletType, FireTransform));
	if (Projectile)
	{
		if (!WeaponParameters.UseLineTraceFire)
		{
			Projectile->ProjectileParameters.Damage = WeaponParameters.Damage;
		}
		Projectile->ProjectileParameters.Speed = WeaponParameters.BulletSpeed;
		Projectile->BulletProjectileMovement->InitialSpeed = WeaponParameters.BulletSpeed;
		Projectile->BulletProjectileMovement->MaxSpeed = WeaponParameters.BulletSpeed;
		Projectile->DamageDelay = WeaponParameters.DamageDelay;
		Projectile->isServer = isServer;
		Projectile->SetOwner(this);
		UGameplayStatics::FinishSpawningActor(Projectile, FireTransform);
	}
}

void ACPP_WeaponParent::LineTraceFire_OnServer_Implementation(FVector FireLocation, FVector EndTrace)
{
	TArray<AActor*> IgnoreActors;
	FHitResult OutHit;
	if (UKismetSystemLibrary::LineTraceSingle(GetWorld(), FireLocation, EndTrace, ETraceTypeQuery::TraceTypeQuery1, false, IgnoreActors, EDrawDebugTrace::None, OutHit, true))
	{
		UGameplayStatics::ApplyDamage(OutHit.GetActor(), WeaponParameters.Damage, GetInstigatorController(), this, NULL);
	}
}

void ACPP_WeaponParent::FireEffects_OnServer_Implementation(FVector FireLocation, FRotator FireRotation)
{
	FireEffects_Multicast(FireLocation, FireRotation);
}

void ACPP_WeaponParent::FireEffects_Multicast_Implementation(FVector FireLocation, FRotator FireRotation)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponParameters.FireSound, FireLocation);
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponParameters.FireParticle, FTransform(FireRotation, FireLocation, FVector(1.0f)));

	//������
	FVector VSleeveLocation(SleeveLocation->GetComponentLocation());
	FTransform SleeveTransform(VSleeveLocation);
	ACPP_Sleeve_Parent* Sleeve = GetWorld()->SpawnActor<ACPP_Sleeve_Parent>(WeaponParameters.SleeveType, SleeveTransform);
	FRotator SleeveRotation = SleeveLocation->GetComponentRotation();
	Sleeve->Capsule->AddImpulse(SleeveRotation.Vector() * 50.f);
}

void ACPP_WeaponParent::Reload()
{
	if (AmmoCurrent >= WeaponParameters.AmmoMax)
	{
		return;
	}
	if (WeaponParameters.UnlimitAmmo)
	{
		AmmoAll = 999;
	}
	if (AmmoAll == 0)
	{
		// �������� ����� �������� ��� ��� ��������
		return;
	}
	
	isReloading = true;
	OwnerOfWeapon->WeaponReloadStartEvent();
	FTimerHandle ReloadTimerHandle;
	GetWorldTimerManager().SetTimer(ReloadTimerHandle, this, &ACPP_WeaponParent::ReloadEnd, WeaponParameters.ReloadTime, false);
}

void ACPP_WeaponParent::ReloadEnd()
{
	int NeedAmmo = WeaponParameters.AmmoMax - AmmoCurrent;
	int AddingAmmo = UKismetMathLibrary::Min(AmmoAll, NeedAmmo);
	AmmoCurrent += AddingAmmo;
	AmmoAll -= AddingAmmo;
	isReloading = false;
	OwnerOfWeapon->WeaponReloadEndEvent();
}

int ACPP_WeaponParent::GetAmmoCurrent()
{
	return AmmoCurrent;
}

void ACPP_WeaponParent::SetAmmoCurrent(int NewAmmoCurrent)
{
	AmmoCurrent = NewAmmoCurrent;
}

int ACPP_WeaponParent::GetAmmoAll()
{
	return AmmoAll;
}

void ACPP_WeaponParent::SetAmmoAll(int NewAmmoAll)
{
	AmmoAll = NewAmmoAll;
}

void ACPP_WeaponParent::AddAmmoAll(int AddingAmmo)
{
	AmmoAll += AddingAmmo;
}

int ACPP_WeaponParent::GetAmmoMax()
{
	return WeaponParameters.AmmoMax;
}

UTexture2D* ACPP_WeaponParent::GetWeaponIcon()
{
	return WeaponParameters.WeaponIcon;
}

UAnimMontage* ACPP_WeaponParent::GetReloadMontage()
{
	return WeaponParameters.ReloadMontage;
}

bool ACPP_WeaponParent::GetIsUnlimitAmmo()
{
	return isUnlimitAmmo;
}

void ACPP_WeaponParent::SetIsServer(bool NewIsServer)
{
	isServer = NewIsServer;
}

void ACPP_WeaponParent::SetIsLocal(bool NewIsLocal)
{
	isLocal = NewIsLocal;
}

