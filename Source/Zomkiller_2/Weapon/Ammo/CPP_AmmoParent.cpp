// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/Ammo/CPP_AmmoParent.h"
#include "Character/CPP_Character.h"
#include "Components/BoxComponent.h"

// Sets default values
ACPP_AmmoParent::ACPP_AmmoParent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box component"));
	BoxComponent->SetGenerateOverlapEvents(true);
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ACPP_AmmoParent::BoxComponentBeginOverlap);

	RootComponent = BoxComponent;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMesh->SetGenerateOverlapEvents(false);
	StaticMesh->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMesh->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ACPP_AmmoParent::BeginPlay()
{
	Super::BeginPlay();
	
	switch (WeaponType)
	{
	case EWeaponType::Rifle:
		Count = FMath::RandRange(30, 100);
		break;
	case EWeaponType::Sniper:
		Count = FMath::RandRange(20, 50);
		break;
	case EWeaponType::Shotgun:
		Count = FMath::RandRange(20, 50);
		break;
	case EWeaponType::GrenadeLauncher:
		Count = FMath::RandRange(2, 5);
		break;
	default: break;
	}

}

// Called every frame
void ACPP_AmmoParent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FRotator Delta = FRotator(0.0f, 3.0f, 0.0f);
	this->AddActorWorldRotation(Delta);
	//StaticMeshWeapon->AddWorldRotation(Delta);
}

void ACPP_AmmoParent::BoxComponentBeginOverlap(
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	ACPP_Character* Player = Cast<ACPP_Character>(OtherActor);
	if (Player != nullptr)
	{
		Player->AddAmmo(WeaponType, Count);
		this->Destroy();
	}
}

