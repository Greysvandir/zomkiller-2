// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/Sleeve/CPP_Sleeve_Parent.h"
#include "Components/CapsuleComponent.h"

// Sets default values
ACPP_Sleeve_Parent::ACPP_Sleeve_Parent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Capsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	Capsule->SetCapsuleRadius(1.f);
	Capsule->SetCapsuleHalfHeight(2.f);
	Capsule->SetCanEverAffectNavigation(false);
	RootComponent = Capsule;

	SleeveMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	SleeveMesh->SetupAttachment(RootComponent);
	SleeveMesh->SetCanEverAffectNavigation(false);
}

// Called when the game starts or when spawned
void ACPP_Sleeve_Parent::BeginPlay()
{
	Super::BeginPlay();
	this->SetLifeSpan(10.f);
}

// Called every frame
void ACPP_Sleeve_Parent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

