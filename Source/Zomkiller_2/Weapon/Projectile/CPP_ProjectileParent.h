// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "FuncLibrary/CPP_Types.h"
#include "Interfaces/CPP_IHitEffect.h"
#include "CPP_ProjectileParent.generated.h"

class UPrimitiveComponent;

UCLASS()
class ZOMKILLER_2_API ACPP_ProjectileParent : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_ProjectileParent();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* BulletMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USphereComponent* BulletCollisionSphere = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UProjectileMovementComponent* BulletProjectileMovement = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UParticleSystemComponent* BulletFX = nullptr;

	float DamageDelay = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = SETTINGS)
	TSubclassOf<UCPP_StatesEffects> HitEffect = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool isServer = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = SETTINGS)
	FProjectileParameters ProjectileParameters;

	UFUNCTION()
	virtual void BulletCollisionSphereHit(
		UPrimitiveComponent* HitComp,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		FVector NormalImpulse,
		const FHitResult& Hit);
	UFUNCTION()
	void BulletCollisionSphereBeginOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);

	void BulletHit(AActor* OtherActor, const FHitResult& Hit);

	UFUNCTION(BlueprintNativeEvent)
	void HitEvent();
	void HitEvent_Implementation();

	void HitByTimer();

	void StartHitEffect(AActor* OtherActor);

	UFUNCTION(NetMulticast, Reliable)
	void SetUnVisible_Multicast();

	UFUNCTION(NetMulticast, Unreliable)
	void SpawnDecalAttached_Multicast(UMaterialInterface* myMaterial, FVector DecalSize, UPrimitiveComponent* OtherComp, FVector ImpactPoint, FRotator Rotation);

	UFUNCTION(NetMulticast, Unreliable)
	void SpawnEmitterAtLocation_Multicast(UParticleSystem* myParticle, FVector ImpactPoint, FRotator Rotation);

	UFUNCTION(NetMulticast, Unreliable)
	void PlaySoundAtLocation_Multicast(USoundBase* HitSoundSurface, FVector ImpactPoint);
};
