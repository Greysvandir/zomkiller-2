// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/Projectile/CPP_ProjectileParent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

// Sets default values
ACPP_ProjectileParent::ACPP_ProjectileParent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	BulletCollisionSphere->SetSphereRadius(16.f);
	BulletCollisionSphere->bReturnMaterialOnMove = true;//hit event return physMaterial
	BulletCollisionSphere->SetCanEverAffectNavigation(false);//collision not affect navigation (P keybord on editor)

	RootComponent = BulletCollisionSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 10.f;
	BulletProjectileMovement->MaxSpeed = 0.f;

	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;

	bReplicates = true;
	
}

// Called when the game starts or when spawned
void ACPP_ProjectileParent::BeginPlay()
{
	Super::BeginPlay();
	this->SetLifeSpan(10.f);

	if (isServer)
	{
		if (DamageDelay == 0.0f)
		{
			BulletCollisionSphere->OnComponentHit.AddDynamic(this, &ACPP_ProjectileParent::BulletCollisionSphereHit);
			BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ACPP_ProjectileParent::BulletCollisionSphereBeginOverlap);
		}
		else
		{
			FTimerHandle ManageShadowTimer;
			GetWorldTimerManager().SetTimer(ManageShadowTimer, this, &ACPP_ProjectileParent::HitByTimer, DamageDelay, false, -1.0f);
		}
	}
}

// Called every frame
void ACPP_ProjectileParent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACPP_ProjectileParent::BulletCollisionSphereHit(
	UPrimitiveComponent* HitComp,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	FVector NormalImpulse,
	const FHitResult& Hit)
{
	// it is always server code

	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

		if (ProjectileParameters.HitDecals.Contains(mySurfacetype))
		{
			UMaterialInterface* myMaterial = ProjectileParameters.HitDecals[mySurfacetype];

			if (myMaterial && OtherComp)
			{
				FVector DecalSize;
				if (!ProjectileParameters.isSphereDamage)
				{
					DecalSize = FVector(5.0f, 20.0f, 20.0f);
				}
				else
				{
					DecalSize = FVector(20.0f, 150.0f, 150.0f);
				}
				//UGameplayStatics::SpawnDecalAttached(myMaterial, DecalSize, OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
				SpawnDecalAttached_Multicast(myMaterial, DecalSize, OtherComp, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
			}
		}
		if (ProjectileParameters.HitFXs.Contains(mySurfacetype))
		{
			UParticleSystem* myParticle = ProjectileParameters.HitFXs[mySurfacetype];
			if (myParticle)
			{
				//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
				SpawnEmitterAtLocation_Multicast(myParticle, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
			}
		}

		if (ProjectileParameters.HitSoundSurface)
		{
			//UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileParameters.HitSoundSurface, Hit.ImpactPoint);
			PlaySoundAtLocation_Multicast(ProjectileParameters.HitSoundSurface, Hit.ImpactPoint);
		}

	}

	BulletHit(OtherActor, Hit);
}

void ACPP_ProjectileParent::BulletCollisionSphereBeginOverlap(
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	// it is always server code

	if (OtherActor && SweepResult.PhysMaterial.IsValid())
	{
		EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(SweepResult);

		if (ProjectileParameters.HitFXs.Contains(mySurfacetype))
		{
			UParticleSystem* myParticle = ProjectileParameters.HitFXs[mySurfacetype];
			if (myParticle)
			{
				//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(SweepResult.ImpactNormal.Rotation(), SweepResult.ImpactPoint, FVector(1.0f)));
				SpawnEmitterAtLocation_Multicast(myParticle, SweepResult.ImpactPoint, SweepResult.ImpactNormal.Rotation());
			}
		}
	}
	
	if (ProjectileParameters.HitSoundBody)
	{
		//UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileParameters.HitSoundBody, SweepResult.ImpactPoint);
		PlaySoundAtLocation_Multicast(ProjectileParameters.HitSoundBody, SweepResult.ImpactPoint);
	}
	BulletHit(OtherActor, SweepResult);
}

void ACPP_ProjectileParent::BulletHit(AActor* OtherActor, const FHitResult& Hit)
{
	// it is always server code
	if (!ProjectileParameters.isSphereDamage)
	{
		UGameplayStatics::ApplyDamage(OtherActor, ProjectileParameters.Damage, GetInstigatorController(), this, NULL);
		StartHitEffect(OtherActor);
	}
	else
	{
		//DrawDebugSphere(GetWorld(), Hit.ImpactPoint, 75.f, 10, FColor::Red, false, 5.f);
		//DrawDebugSphere(GetWorld(), Hit.ImpactPoint, 125.f, 10, FColor::Yellow, false, 5.f);
		
		FVector Location = GetActorLocation();
		//DrawDebugSphere(GetWorld(), Location, 75.f, 10, FColor::Red, false, 5.f);
		//DrawDebugSphere(GetWorld(), Location, 125.f, 10, FColor::Yellow, false, 5.f);
		
		TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
		//ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));
		TArray<AActor*> ActorsToIgnore;
		ActorsToIgnore.Add(this);
		TArray<AActor*> OutActors;

		// ������ ���� � ������ ������
		UKismetSystemLibrary::SphereOverlapActors(GetWorld(), Location, 75.f, ObjectTypes, AActor::StaticClass(), ActorsToIgnore, OutActors);
		for (int i = 0; i < OutActors.Num(); i++)
		{
			UGameplayStatics::ApplyDamage(OutActors[i], ProjectileParameters.Damage, GetInstigatorController(), this, NULL);
			StartHitEffect(OutActors[i]);
			ActorsToIgnore.Add(OutActors[i]);
		}
		
		// �������� ����� �� ����� ������
		UKismetSystemLibrary::SphereOverlapActors(GetWorld(), Location, 125.f, ObjectTypes, AActor::StaticClass(), ActorsToIgnore, OutActors);
		for (int i = 0; i < OutActors.Num(); i++)
		{
			UGameplayStatics::ApplyDamage(OutActors[i], ProjectileParameters.Damage / 2.f, GetInstigatorController(), this, NULL);
			StartHitEffect(OutActors[i]);
		}
	}

	BulletCollisionSphere->SetGenerateOverlapEvents(false);
	BulletCollisionSphere->SetCollisionProfileName(TEXT("NoCollision"));
	SetUnVisible_Multicast();
	BulletFX->DeactivateSystem();
	FHitResult HitResult;
	BulletProjectileMovement->StopSimulating(HitResult);
	HitEvent();
}

void ACPP_ProjectileParent::HitEvent_Implementation()
{
	//��� ��������������� � BP
}

void ACPP_ProjectileParent::HitByTimer()
{
	// it is always server code

	FVector Location = GetActorLocation();
	EPhysicalSurface mySurfacetype = EPhysicalSurface::SurfaceType_Default;
	UParticleSystem* myParticle = ProjectileParameters.HitFXs[mySurfacetype];
	if (myParticle)
	{
		//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(FRotator(0.0f, 0.0f, 0.0f), Location, FVector(1.0f)));
		SpawnEmitterAtLocation_Multicast(myParticle, Location, FRotator(0.0f, 0.0f, 0.0f));
	}

	if (ProjectileParameters.HitSoundSurface)
	{
		//UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileParameters.HitSoundSurface, Location);
		PlaySoundAtLocation_Multicast(ProjectileParameters.HitSoundSurface, Location);
	}

	FHitResult Hit;
	BulletHit(this, Hit);
}

void ACPP_ProjectileParent::StartHitEffect(AActor* OtherActor)
{
	// ���� ��� ������� �� ������ ������, �� �������
	if (HitEffect == nullptr)
	{
		return;
	}

	// ������ ������. ��������� ���� �� � �������, � ������� ������, ��������� ��������� ��������� ��������
	ICPP_IHitEffect* Iface = Cast<ICPP_IHitEffect>(OtherActor);
	if (Iface != nullptr)
	{
		Iface->StartEffect(HitEffect);
	}
}

void ACPP_ProjectileParent::PlaySoundAtLocation_Multicast_Implementation(USoundBase* HitSoundSurface, FVector ImpactPoint)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSoundSurface, ImpactPoint);
}

void ACPP_ProjectileParent::SpawnEmitterAtLocation_Multicast_Implementation(UParticleSystem* myParticle, FVector ImpactPoint, FRotator Rotation)
{
	//UGameplayStatics::SpawnEmitterAtLocation(GetOwner()->GetWorld(), myParticle, FTransform(Rotation, ImpactPoint, FVector(1.0f)));
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Rotation, ImpactPoint, FVector(1.0f)));
}

void ACPP_ProjectileParent::SpawnDecalAttached_Multicast_Implementation(UMaterialInterface* myMaterial, FVector DecalSize, UPrimitiveComponent* OtherComp, FVector ImpactPoint, FRotator Rotation)
{
	UGameplayStatics::SpawnDecalAttached(myMaterial, DecalSize, OtherComp, NAME_None, ImpactPoint, Rotation, EAttachLocation::KeepWorldPosition, 10.0f);
}

void ACPP_ProjectileParent::SetUnVisible_Multicast_Implementation()
{
	BulletMesh->SetVisibility(false);
	BulletFX->SetVisibility(false);
}
