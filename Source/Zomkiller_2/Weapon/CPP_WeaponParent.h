// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FuncLibrary/CPP_Types.h"
#include "CPP_WeaponParent.generated.h"

class UBoxComponent;
class USkeletalMeshComponent;
class UStaticMeshComponent;
class UArrowComponent;
class ACPP_Character;

UCLASS()
class ZOMKILLER_2_API ACPP_WeaponParent : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_WeaponParent();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
	UBoxComponent* BoxComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UArrowComponent* ShootLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UArrowComponent* SleeveLocation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = SETTINGS)
	FName IdWeaponName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = SETTINGS)
	FName EquipSocket = "RightArm";
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = ReadOnly)
	FWeaponParameters WeaponParameters;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = ReadOnly)
	ACPP_Character* OwnerOfWeapon;

	UPROPERTY()
	bool inFire = false;

	bool inCooldown = false;
	float CooldownTime = 0.f;
	bool isReloading = false;
	int AmmoCurrent = 0; // ������� � ������
	int AmmoAll = 0; // ������� � ���������
	bool isUnlimitAmmo = false;

	UPROPERTY(BlueprintReadOnly)
	bool isServer = false;

	UPROPERTY(BlueprintReadWrite)
	bool isLocal = false;

	void SetWeaponParameters();

	UFUNCTION(BlueprintCallable)
	void Equip(ACPP_Character* NewOwner, int NewAmmoCurrent, int NewAmmoAll);

	UFUNCTION(BlueprintCallable)
	void StartFire();

	UFUNCTION(BlueprintCallable)
	void StopFire();

	UFUNCTION(Server, Reliable)
	void StartFire_OnServer();

	UFUNCTION(Server, Reliable)
	void StopFire_OnServer();

	UFUNCTION(NetMulticast, Reliable)
	void StartFire_Multicast();

	UFUNCTION(NetMulticast, Reliable)
	void StopFire_Multicast();

	UFUNCTION(BlueprintCallable)
	void Fire(float DeltaTime);

	UFUNCTION(Server, Reliable)
	void Fire_OnServer(FTransform FireTransform);

	UFUNCTION(Server, Reliable)
	void LineTraceFire_OnServer(FVector FireLocation, FVector EndTrace);

	UFUNCTION(Server, Unreliable)
	void FireEffects_OnServer(FVector FireLocation, FRotator FireRotation);

	UFUNCTION(NetMulticast, Unreliable)
	void FireEffects_Multicast(FVector FireLocation, FRotator FireRotation);

	UFUNCTION(BlueprintCallable)
	void Reload();
	void ReloadEnd();

	UFUNCTION(BlueprintPure)
	int GetAmmoCurrent();

	UFUNCTION(BlueprintCallable)
	void SetAmmoCurrent(int NewAmmoCurrent);

	UFUNCTION(BlueprintPure)
	int GetAmmoAll();

	UFUNCTION(BlueprintCallable)
	void SetAmmoAll(int NewAmmoAll);

	UFUNCTION(BlueprintCallable)
	void AddAmmoAll(int AddingAmmo);

	UFUNCTION(BlueprintPure)
	int GetAmmoMax();

	UFUNCTION(BlueprintPure)
	UTexture2D* GetWeaponIcon();

	UFUNCTION(BlueprintPure)
	UAnimMontage* GetReloadMontage();

	UFUNCTION(BlueprintPure)
	bool GetIsUnlimitAmmo();

	UFUNCTION(BlueprintCallable)
	void SetIsServer(bool NewIsServer);

	UFUNCTION(BlueprintCallable)
	void SetIsLocal(bool NewIsLocal);
};
