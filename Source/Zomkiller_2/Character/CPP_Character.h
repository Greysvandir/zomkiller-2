// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FuncLibrary/CPP_Types.h"
#include "Interfaces/CPP_IHitEffect.h"
#include "StatesEffects/CPP_StatesEffectsCommonFunc.h"
#include "CPP_Character.generated.h"

class ACPP_WeaponParent;

UCLASS(Blueprintable)
class ACPP_Character : public ACharacter, public ICPP_IHitEffect
{
	GENERATED_BODY()

public:
	ACPP_Character();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	bool isDead = false;

public:
	APlayerController* PlayerController;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool AimEnabled = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", Replicated)
	EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed SpeedInfo;

	// ����� �������� � ������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	TMap<TSubclassOf<ACPP_WeaponParent>, int> AmmoCurrent;
	// ����� �������� � ���������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	TMap<TSubclassOf<ACPP_WeaponParent>, int> AmmoAll;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffects")
	TArray<TEnumAsByte<EPhysicalSurface>> ObjectSurfaces;

	TArray<FStateEffectStack> CurrentStates;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool isServer;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void InputAxisX(float Value);

	UFUNCTION()
	void InputAxisY(float Value);

	UFUNCTION()
	void InputAxisMovement(FVector MoveDirection, float Value);

	UFUNCTION()
	void RotatePlayerToMouse();

	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	ACPP_WeaponParent* CurrentWeapon;

	UFUNCTION(BlueprintNativeEvent)
	void WeaponEquipEvent();
	void WeaponEquipEvent_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireEvent();
	void WeaponFireEvent_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStartEvent();
	void WeaponReloadStartEvent_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEndEvent();
	void WeaponReloadEndEvent_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	void AddAmmo(EWeaponType WeaponType, int Count);
	void AddAmmo_Implementation(EWeaponType WeaponType, int Count);

	UFUNCTION(BlueprintCallable)
	void SetIsDead(bool NewIsDead);

	UFUNCTION(BlueprintPure)
	bool GetIsDead();

	void StartEffect(TSubclassOf<UCPP_StatesEffects> NewState) override;
	void StopEffect(TSubclassOf<UCPP_StatesEffects> NewState) override;

	UFUNCTION(BlueprintPure)
	bool IsLocal();

	UFUNCTION(Server, Unreliable)
	void SetActorRotationByYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
	void SetMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
	void SetMovementState_Multicast(EMovementState NewState);
};

