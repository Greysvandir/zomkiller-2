// Copyright Epic Games, Inc. All Rights Reserved.

#include "CPP_Character.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "Net/UnrealNetwork.h"

#define TraceMouseCursor ETraceTypeQuery::TraceTypeQuery3 // ������ � ������, � ���� ��� ����� �� 3 ������, � �� 6

ACPP_Character::ACPP_Character()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ACPP_Character::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	/*if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}*/

	if (isDead)
	{
		return;
	}
	RotatePlayerToMouse();
}

void ACPP_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ACPP_Character::InputAxisX);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ACPP_Character::InputAxisY);
}

void ACPP_Character::InputAxisX(float Value)
{
	if (isDead)
	{
		return;
	}
	//AddMovementInput(FVector(1.f, 0.f, 0.f), Value);
	InputAxisMovement(FVector(1.f, 0.f, 0.f), Value);
}

void ACPP_Character::InputAxisY(float Value)
{
	if (isDead)
	{
		return;
	}
	//AddMovementInput(FVector(0.f, 1.f, 0.f), Value);
	InputAxisMovement(FVector(0.f, 1.f, 0.f), Value);
}

void ACPP_Character::InputAxisMovement(FVector MoveDirection, float Value)
{
	if (!IsLocal())
	{
		return;
	}

	if (!SprintRunEnabled)
	{
		AddMovementInput(MoveDirection, Value);
	}
	else
	{
		AddMovementInput(GetActorForwardVector(), 1);
	}
}

void ACPP_Character::RotatePlayerToMouse()
{
	if (!IsLocal())
	{
		return;
	}

	if (PlayerController)
	{
		FHitResult HitResult;
		PlayerController->GetHitResultUnderCursorByChannel(TraceMouseCursor, false, HitResult);
		float Yaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw;
		SetActorRotation(FRotator(0.f, Yaw, 0.f));
		SetActorRotationByYaw_OnServer(Yaw);
	}
	else
	{
		PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	}
}

void ACPP_Character::CharacterUpdate()
{
	if (!IsLocal())
	{
		return;
	}

	float ResSpeed = 600.f;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = SpeedInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = SpeedInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:
		ResSpeed = SpeedInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResSpeed = SpeedInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = SpeedInfo.SprintRunSpeedRun;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ACPP_Character::ChangeMovementState()
{
	if (!IsLocal())
	{
		return;
	}

	if (isDead)
	{
		return;
	}
	EMovementState NewState = EMovementState::Run_State;
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		NewState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			NewState = EMovementState::SprintRun_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && AimEnabled)
			{
				NewState = EMovementState::AimWalk_State;
			}
			else
			{
				if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
				{
					NewState = EMovementState::Walk_State;
				}
				else
				{
					if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
					{
						NewState = EMovementState::Aim_State;
					}
				}
			}
		}
	}

	//MovementState = NewState;
	//CharacterUpdate();
	SetMovementState_OnServer(NewState);
}

void ACPP_Character::WeaponEquipEvent_Implementation()
{
}

void ACPP_Character::WeaponFireEvent_Implementation()
{
}

void ACPP_Character::WeaponReloadStartEvent_Implementation()
{
}

void ACPP_Character::WeaponReloadEndEvent_Implementation()
{
}

void ACPP_Character::AddAmmo_Implementation(EWeaponType WeaponType, int Count)
{
}

void ACPP_Character::SetIsDead(bool NewIsDead)
{
	isDead = NewIsDead;
}

bool ACPP_Character::GetIsDead()
{
	return isDead;
}

void ACPP_Character::StartEffect(TSubclassOf<UCPP_StatesEffects> NewState)
{
	if (isServer)
	{
		UCPP_StatesEffectsCommonFunc::CheckSurfaceAndStartEffect(ObjectSurfaces, CurrentStates, NewState, this);
	}
}

void ACPP_Character::StopEffect(TSubclassOf<UCPP_StatesEffects> NewState)
{
	if (isServer)
	{
		UCPP_StatesEffectsCommonFunc::RemoveStatesByClass(CurrentStates, NewState);
	}
}

bool ACPP_Character::IsLocal()
{
	if (Controller && Controller->IsLocalPlayerController())
	{
		return true;
	}
	return false;
}

void ACPP_Character::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}

void ACPP_Character::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	if (!IsLocal())
	{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void ACPP_Character::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
	SetMovementState_Multicast(NewState);
}

void ACPP_Character::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdate();
}

void ACPP_Character::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACPP_Character, MovementState);
}