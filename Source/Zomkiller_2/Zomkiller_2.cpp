// Copyright Epic Games, Inc. All Rights Reserved.

#include "Zomkiller_2.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Zomkiller_2, "Zomkiller_2" );

DEFINE_LOG_CATEGORY(LogZomkiller_2)
DEFINE_LOG_CATEGORY(LogZomkiller_2_Net)
 