// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogZomkiller_2, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogZomkiller_2_Net, Log, All);
