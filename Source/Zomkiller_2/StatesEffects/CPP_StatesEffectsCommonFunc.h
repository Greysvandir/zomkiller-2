// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FuncLibrary/CPP_Types.h"
#include "CPP_StatesEffectsCommonFunc.generated.h"

/**
 * 
 */
UCLASS()
class ZOMKILLER_2_API UCPP_StatesEffectsCommonFunc : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	static void CheckSurfaceAndStartEffect(
		TArray<TEnumAsByte<EPhysicalSurface>> ObjectSurfaces,
		TArray<FStateEffectStack>& CurrentStates,
		TSubclassOf<UCPP_StatesEffects> NewState,
		AActor* StateOwner
	);
	static void AddEffectToStack(
		TArray<FStateEffectStack> &CurrentStates,
		TSubclassOf<UCPP_StatesEffects> NewState,
		AActor* StateOwner
	);
	static UCPP_StatesEffects* CreateAndStartEffect(
		TSubclassOf<UCPP_StatesEffects> NewState,
		AActor* StateOwner
	);
	static void RemoveStatesByClass(
		TArray<FStateEffectStack>& CurrentStates,
		TSubclassOf<UCPP_StatesEffects> NewState
	);
};
