// Fill out your copyright notice in the Description page of Project Settings.


#include "StatesEffects/CPP_EffectEmitter.h"
#include "Net/UnrealNetwork.h"

ACPP_EffectEmitter::ACPP_EffectEmitter()
{
    bReplicates = true;
}

void ACPP_EffectEmitter::BeginPlay()
{
    Super::BeginPlay();
    SetOwner(IOwner);
    StartEmitterEffect();
}

void ACPP_EffectEmitter::StartEmitterEffect()
{
    FName NameBoneToAttached;
    FVector Loc = FVector(0);

    ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, IOwner->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
}

void ACPP_EffectEmitter::StopEmitterEffect(bool DoDelayDeactivate)
{
    if (DoDelayDeactivate)
    {
        //GetWorld()->GetTimerManager().SetTimer(DelayDeactivateTimer, this, &ACPP_EffectEmitter::DelayDeactivate, 3.f, false);
        SetDelayDeactivateTimer_Multicast();
        GetWorld()->GetTimerManager().SetTimer(AutoDestroyTimer, this, &ACPP_EffectEmitter::AutoDestroy, 8.f, false);
    }
    else
    {
        //ParticleEmitter->Deactivate();
        ParticleEmitterDeactivate_Multicast();
        GetWorld()->GetTimerManager().SetTimer(AutoDestroyTimer, this, &ACPP_EffectEmitter::AutoDestroy, 5.f, false);
    }
}

void ACPP_EffectEmitter::DelayDeactivate()
{
    UWorld* World = IOwner->GetWorld();
    //UWorld* World = GetWorld();
    if (World)
    {
        if (World->GetTimerManager().IsTimerActive(DelayDeactivateTimer))
        {
            World->GetTimerManager().ClearTimer(DelayDeactivateTimer);
        }
    }

    if (ParticleEmitter)
    {
        ParticleEmitter->Deactivate();
    }
}

void ACPP_EffectEmitter::AutoDestroy()
{
    UWorld* World = IOwner->GetWorld();
    if (World)
    {
        if (World->GetTimerManager().IsTimerActive(AutoDestroyTimer))
        {
            World->GetTimerManager().ClearTimer(AutoDestroyTimer);
        }
    }

    if (ParticleEmitter)
    {
        ParticleEmitter->DestroyComponent();
        ParticleEmitter = nullptr;
    }

    Destroy();
}

void ACPP_EffectEmitter::ParticleEmitterDeactivate_Multicast_Implementation()
{
    ParticleEmitter->Deactivate();
}

void ACPP_EffectEmitter::SetDelayDeactivateTimer_Multicast_Implementation()
{
    GetWorld()->GetTimerManager().SetTimer(DelayDeactivateTimer, this, &ACPP_EffectEmitter::DelayDeactivate, 3.f, false);
}

void ACPP_EffectEmitter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    // ���������, ��� ���������� SomeVariable ������ ���������������
    DOREPLIFETIME(ACPP_EffectEmitter, IOwner);
    DOREPLIFETIME(ACPP_EffectEmitter, ParticleEffect);
}