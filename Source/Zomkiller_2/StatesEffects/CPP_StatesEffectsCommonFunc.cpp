// Fill out your copyright notice in the Description page of Project Settings.


#include "StatesEffects/CPP_StatesEffectsCommonFunc.h"

void UCPP_StatesEffectsCommonFunc::CheckSurfaceAndStartEffect(
	TArray<TEnumAsByte<EPhysicalSurface>> ObjectSurfaces,
	TArray<FStateEffectStack>& CurrentStates,
	TSubclassOf<UCPP_StatesEffects> NewState,
	AActor* StateOwner)
{
	if (!NewState.GetDefaultObject()->SuitableSurface(ObjectSurfaces, NewState))
	{
		return;
	}

	// ����������� ��������, ��������� ���� �� ��� ����� ������ �� ������, ����� �� ��������, ����� �� ��������, ��������� ������
	AddEffectToStack(CurrentStates, NewState, StateOwner);
}

void UCPP_StatesEffectsCommonFunc::AddEffectToStack(
	TArray<FStateEffectStack> &CurrentStates,
	TSubclassOf<UCPP_StatesEffects> NewState,
	AActor* StateOwner)
{
	UCPP_StatesEffects* DefaultObject = NewState.GetDefaultObject();

	bool ExecOnce = DefaultObject->ExecOnce();
	if (ExecOnce)
	{
		CreateAndStartEffect(NewState, StateOwner);
		return; // ��������� ��� ������� ������ ���������, ����� �� ���������
	}

	int MaxStackForNewState = DefaultObject->MaxStack;

	for (FStateEffectStack &CurrentState : CurrentStates)
	{
		if (CurrentState.StateClass == NewState)
		{
			if (DefaultObject->DoUpdate)
			{
				if (CurrentState.StateRef != nullptr)
				{
					CurrentState.StateRef->UpdateEffect();
				}
			}

			if (CurrentState.Stack < MaxStackForNewState)
			{
				CurrentState.Stack++;
				if (CurrentState.StateRef != nullptr)
				{
					CurrentState.StateRef->CurrentStack = CurrentState.Stack;
				}
				return;
			}
			else
			{
				return;
			}
		}
	}

	FStateEffectStack AddingState;
	AddingState.Stack = 1;
	AddingState.StateClass = NewState;

	UCPP_StatesEffects* NewEffect = CreateAndStartEffect(NewState, StateOwner);
	AddingState.StateRef = NewEffect;

	CurrentStates.Add(AddingState);
}

UCPP_StatesEffects* UCPP_StatesEffectsCommonFunc::CreateAndStartEffect(TSubclassOf<UCPP_StatesEffects> NewState, AActor* StateOwner)
{
	UCPP_StatesEffects* NewEffect = NewObject<UCPP_StatesEffects>(StateOwner, NewState);
	if (NewEffect)
	{
		NewEffect->StartEffect(StateOwner);
	}
	return NewEffect;
}

void UCPP_StatesEffectsCommonFunc::RemoveStatesByClass(TArray<FStateEffectStack>& CurrentStates, TSubclassOf<UCPP_StatesEffects> NewState)
{
	CurrentStates.RemoveAll([NewState](const FStateEffectStack& StackElement)
		{
			return StackElement.StateClass == NewState;
		}
	);
}
