// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "CPP_EffectEmitter.generated.h"

UCLASS()
class ZOMKILLER_2_API ACPP_EffectEmitter : public AActor
{
	GENERATED_BODY()
	
public:	
	ACPP_EffectEmitter();
	UParticleSystemComponent* ParticleEmitter = nullptr;
	FTimerHandle AutoDestroyTimer;
	FTimerHandle DelayDeactivateTimer;

	UPROPERTY(Replicated)
	AActor* IOwner;

	UPROPERTY(Replicated)
	UParticleSystem* ParticleEffect;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	void StartEmitterEffect();
	void StopEmitterEffect(bool DoDelayDeactivate);
	void DelayDeactivate();
	void AutoDestroy();

	UFUNCTION(NetMulticast, Reliable)
	void SetDelayDeactivateTimer_Multicast();

	UFUNCTION(NetMulticast, Reliable)
	void ParticleEmitterDeactivate_Multicast();
};
