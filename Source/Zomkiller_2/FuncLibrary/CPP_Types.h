// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "CPP_StatesEffects.h"
#include "CPP_Types.generated.h"

class ACPP_ProjectileParent;
class ACPP_Sleeve_Parent;

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	AimWalk_State UMETA(DisplayName = "AimWalk State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
	SprintRun_State UMETA(DisplayName = "SprintRun State")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float AimSpeedNormal = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float WalkSpeedNormal = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float RunSpeedNormal = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float AimSpeedWalk = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float SprintRunSpeedRun = 800.0f;
};

USTRUCT(BlueprintType)
struct FWeaponParameters : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	float Damage = 50.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	float Cooldown = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	float BulletSpeed = 1000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	int AmmoMax = 30;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	float ReloadTime = 1.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	float Dispersion = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	bool BlockAimDispersion = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	int BulletsPerFire = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	bool UseLineTraceFire = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	TSubclassOf<ACPP_ProjectileParent> BulletType = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	USoundBase* FireSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	UParticleSystem* FireParticle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	UTexture2D* WeaponIcon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	UAnimMontage* ReloadMontage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	TSubclassOf<ACPP_Sleeve_Parent> SleeveType = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	bool UnlimitAmmo = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	float DamageDelay = 0.0f;
};

USTRUCT(BlueprintType)
struct FProjectileParameters
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	float Damage = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	float Speed = 0.5f;
	//material to decal on hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	//fx when hit check by surface
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;
	//Sound when hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	USoundBase* HitSoundSurface = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	USoundBase* HitSoundBody = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	bool isSphereDamage = false;
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	Rifle UMETA(DisplayName = "Rifle"),
	Sniper UMETA(DisplayName = "Sniper"),
	Shotgun UMETA(DisplayName = "Shotgun"),
	GrenadeLauncher UMETA(DisplayName = "Grenade launcher")
};

USTRUCT(BlueprintType)
struct FStateEffectStack
{
	GENERATED_BODY()

	TSubclassOf<UCPP_StatesEffects> StateClass;
	UCPP_StatesEffects* StateRef;
	int Stack;
};

UCLASS()
class ZOMKILLER_2_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
