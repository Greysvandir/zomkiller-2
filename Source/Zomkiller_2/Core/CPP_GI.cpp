// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/CPP_GI.h"

bool UCPP_GI::GetWeaponInfoByName(FName NameWeapon, FWeaponParameters& OutInfo)
{
	bool bIsFind = false;
	FWeaponParameters* WeaponInfoRow;

	if (WeaponInfoTable)
	{
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponParameters>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("CPP_GI::GetWeaponInfoByName - WeaponTable -NULL"));
	}

	return bIsFind;
}
